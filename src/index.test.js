// import files
import add from './calculator/add';
import subtract from './calculator/subtract';
import multiply from './calculator/multiply';
import divide from './calculator/divide';


// Test Case 1 (Addition)
test("Should add two numbers", () => {
  expect(add(25, 10)).toBe(35);
});

// Test Case 2 (Subtraction)
test("Should subtract two numbers", () => {
  expect(subtract(25, 10)).toBe(15);
});


// Test Case 3 (Multiplication)
test("Should multiply two numbers", () => {
  expect(multiply(25, 10)).toBe(250);
});


// Test Case 4 (Division)
test("Should divide two numbers", () => {
  expect(divide(25, 5)).toBe(5);
});
